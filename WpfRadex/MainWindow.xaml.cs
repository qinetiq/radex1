﻿using Radex.Database;
using Radex.Model;
using Radex.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfRadex
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public RadexDatabase Database { get; private set; } = new RadexDatabase();
        public Consumer Consumer { get; private set; } = new Consumer();


        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            Consumer.DataReceived += Consumer_DataReceived;

        }

        private void Consumer_DataReceived(object? sender, SignalData e)
        {
            this.Dispatcher.Invoke(() =>
            {
                lstLive.Items.Add(e);
            });

        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
           Consumer.DataReceived -= Consumer_DataReceived;
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            lstHistory.Items.Clear();

            var datums = Database.GetData();

            foreach (var datum in datums)
            {
                lstHistory.Items.Add(datum);
            }
        }
    }
}
