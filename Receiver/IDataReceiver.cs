﻿namespace Radex.Receiver
{
    public interface IDataReceiver
    {
        public int GetSignalStrength();
    }
}
