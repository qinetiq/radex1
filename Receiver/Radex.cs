﻿using System;
using System.Runtime.InteropServices;

namespace Radex.Receiver
{
    static internal class Radex
    {
        [DllImport("Radex", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        static extern int GetSignalStrength();

        static public int GetSample()
        {
            int r = GetSignalStrength();
            return r;
        }
    }
}
