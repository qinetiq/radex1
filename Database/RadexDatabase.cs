﻿using Dapper;
using Database;
using Npgsql;
using Radex.Model;
using System;
using System.Collections.Generic;

namespace Radex.Database
{
    public class RadexDatabase
    {
        public RadexDatabase(DatabaseConfig config)
        {
            ConnString = config.ConnString;
        }
        public string ConnString { get; set; } = "Server=localhost;Port=5432;User Id=postgres;Password=admin;Database=test1";
        public string TableName { get; set; } = "schema1.data1";
        public void SaveData(SignalData datum)
        {
            var sql = $"insert into {TableName} (item, sourceid) values ('{datum.Signal}', '{datum.SourceId}')";
            using (var connection = new NpgsqlConnection(ConnString))
            {
                var affectedRows = connection.Execute(sql);
                Console.WriteLine($"Affected Rows: {affectedRows}");
            }
        }

        public List<SignalData> GetData()
        {
            var list = new List<SignalData>();
            var sql = $"select * from {TableName}";

            using (var connection = new NpgsqlConnection(ConnString))
            {
                var datums = connection.Query<SignalData>(sql);
                foreach (var datum in datums)
                {
                    list.Add(datum);
                }
            }
            return list;
        }
    }
}
