﻿using System;

namespace Radex.Model
{
    public class SignalData
    {
        public int Signal { get; set; }
        public string SourceId { get; set; } 
        public DateTime TimeStamp{ get; set; }
    }
}
