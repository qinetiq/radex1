﻿using Radex.Queue;

namespace Radex.Producer
{
    public class DataProducerConfig
    {
        public int Interval { get; set; }
        public string  SourceId { get; set; }
    }
}
