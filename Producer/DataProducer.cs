﻿using Radex.Receiver;
using Radex.Queue;


namespace Radex.Producer
{
    public class DataProducer
    {
        // private Timer _timer;
        IDataReceiver _receiver;
        QProducer _producer;
        public int Interval { get; set; } = 5000;
        public string SourceId { get; set; }

        public DataProducer(IDataReceiver receiver,IQueue queue, DataProducerConfig config)
        {
            Interval = config.Interval;
            SourceId = config.SourceId;
            _receiver = receiver;
            _producer = new QProducer(queue);
        }

       // public void Start()
       // {
       //     //_timer = new Timer(OnInteval, null, Interval, Timeout.Infinite);
       // }
       // public void Stop()
       // {
       ////     _timer.Change(Timeout.Infinite, Timeout.Infinite);
       // }

        public void OnInterval()
        {
            int result = _receiver.GetSignalStrength();
            _producer.Send(SourceId, result);
         //   _timer.Change(Interval, Timeout.Infinite);
        }

    }



}
