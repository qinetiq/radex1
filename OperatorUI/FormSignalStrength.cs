﻿using Radex.Queue;
using System;
using System.Windows.Forms;


namespace WindowsFormsCharts
{
    public partial class FormSignalStrength : Form
    {
        QConsumer _consumer;
        public FormSignalStrength(QConsumer consumer)
        {
            _consumer = consumer;
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            chart1.Series["SignalStrength"].Points.AddXY(DateTime.Now.ToString(), "10000");
            chart1.Series["SignalStrength"].Points.AddXY("20", "8000");
            chart1.Series["SignalStrength"].Points.AddXY("30", "7000");
            chart1.Series["SignalStrength"].Points.AddXY("50", "10000");
            chart1.Series["SignalStrength"].Points.AddXY("60", "8500");
            //chart title  
            chart1.Titles.Add("Signal Strength Chart");
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            _consumer.DataReceived += _consumer_DataReceived;
        }

        private void _consumer_DataReceived(object? sender, SignalDto e)
        {
            Invoke(new Action(() =>
            {
                chart1.Series["SignalStrength"].Points.AddXY(e.TimeStamp.ToString(), e.SignalStrength);
            }));
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _consumer.DataReceived -= _consumer_DataReceived;
        }
       
    }
}
