using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Radex.Queue;
using System;
using System.Windows.Forms;
using  WindowsFormsCharts;

namespace OperatorUI
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                  .AddJsonFile($"appsettings.json", true, true);
            var config = builder.Build();
            var section = config.GetSection(nameof(Queue));
            var queueConfig = section.Get<QueueConfig>();

            //using IHost host = Host.CreateDefaultBuilder(args)
            //    .ConfigureServices((_, services) =>
            //    services.AddSingleton<IQueueReceiver, QueueReceiver>())
            //    .Build();

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormSignalStrength(new QConsumer(new QueueReceiver(queueConfig))));
        }
    }
}
