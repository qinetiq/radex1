﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Radex.Producer;
using Radex.Queue;
using Radex.Receiver;
using System;

namespace ProducerService
{
    public static class ProducerServiceCollectionExtensions
    {
        public static IServiceCollection AddProducerService(this IServiceCollection collection, IConfiguration dconfig, IConfiguration qconfig)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            if (dconfig == null) throw new ArgumentNullException(nameof(dconfig));
            if (qconfig == null) throw new ArgumentNullException(nameof(qconfig));

            collection.AddSingleton(dconfig.Get<DataProducerConfig>());
            collection.AddSingleton(qconfig.Get<QueueConfig>());

            collection.AddSingleton<IDataReceiver,DataReceiver>();
            collection.AddSingleton<IQueue, Queue>();

            return collection.AddSingleton<DataProducer>();
        }
    }
}

