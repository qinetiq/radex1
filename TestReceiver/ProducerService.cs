﻿using Radex.Producer;
using System;
using Topshelf;

namespace TestProducer
{
    internal class ProducerService : ServiceControl
    {
        DataProducer _producer;

        public ProducerService(DataProducer producer)
        {
            _producer = producer;
        }

        public bool Start(HostControl hostControl)
        {
            _producer.Start();
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _producer.Stop();
            return true;
        }
    }
}
