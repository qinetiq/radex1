﻿using Microsoft.Extensions.Configuration;
using Radex.Producer;
using System;

namespace TestProducer
{
    internal class ProgramProducer
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                   .AddJsonFile($"appsettings.json", true, true);

            var config = builder.Build();
            var section = config.GetSection(nameof(DataProducer));
            var producerConfig = section.Get<DataProducerConfig>();

            Console.WriteLine("Testing the Radex Receiver...");
            Console.WriteLine("Press any key to exit...");

            var producer = new DataProducer(producerConfig);
            producer.Start();

            Console.ReadKey();
            producer.Stop();

        }
    }
}
