using Moq;
using Radex.Producer;
using Radex.Queue;
using Radex.Receiver;
using Xunit;

namespace Radex.RadexTest
{
    public class TestProducer
    {
        [Fact]
        public void TestProducerProduces()
        {
            //Arrange
            QueueConfig queueConfig = new QueueConfig()
            {
                Exchange = "Test",
                Topic = "test1"
            };

            DataProducerConfig config = new DataProducerConfig()
            {
                Interval = 1000,
                SourceId = "H1",
            };

            Mock<IDataReceiver> mockR = new Mock<IDataReceiver>();
            Mock<IQueue> mockQ = new Mock<IQueue>();

            mockR.Setup(p => p.GetSignalStrength()).Returns(50);

            DataProducer producer = new DataProducer(mockR.Object,mockQ.Object, config);

            //Act
            producer.OnInterval();

            //Assert
        }

    }
}
