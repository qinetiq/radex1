﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;
using System;

namespace Radex.Queue
{
    public class QueueReceiver : Queue, IQueueReceiver
    {
        public QueueReceiver(QueueConfig config) : base(config)
        {
            ExchangeName = config.Exchange;
            RouteName = config.Topic;

            var queueName = Channel.QueueDeclare().QueueName;
            Channel.QueueBind(queue: queueName,
                              exchange: ExchangeName,
                              routingKey: RouteName);

            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += Consumer_Received;
            Channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);
        }

        public event EventHandler<string> DataReceived;
        private void Consumer_Received(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            EventHandler<string> handler = DataReceived;
            if (null != handler) handler(this, message);
        }
    }
}
