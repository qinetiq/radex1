﻿using Newtonsoft.Json;
using System;

namespace Radex.Queue
{
    public class QConsumer
    {
        public IQueueReceiver Queue { get; private set; }

        public event EventHandler<SignalDto> DataReceived;

        public QConsumer(IQueueReceiver receiver)
        {
            Queue = receiver;
            Queue.DataReceived += Queue_DataReceived;
        }

        private void Queue_DataReceived(object sender, string message)
        {
            var dto = JsonConvert.DeserializeObject<SignalDto>(message);
            Console.WriteLine(" [x] Received {0}", message);
            EventHandler<SignalDto> handler = DataReceived;
            if (null != handler) handler(this, dto);
        }
    }
}
