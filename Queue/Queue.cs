﻿using RabbitMQ.Client;
using System.Text;

namespace Radex.Queue
{
    public class Queue : IQueue
    {
        public Queue(QueueConfig config)
        {
            ExchangeName = config.Exchange;
            RouteName = config.Topic;
            var factory = new ConnectionFactory() { HostName = "localhost" };
            Connection = factory.CreateConnection();
            Channel = Connection.CreateModel();

            Channel.ExchangeDeclare(ExchangeName, ExchangeType.Topic);

        }
        public IConnection Connection { get; private set; }
        public  IModel Channel { get; private set; }
        public string QueueName { get; private set; } = "Radex";
        public string ExchangeName { get;  set; } = "Radex";
        public string RouteName { get;  set; } = "Radex";

        //public Queue()
        //{

        //    //Channel.QueueDeclare(queue: QueueName,
        //    //                         durable: false,
        //    //                         exclusive: false,
        //    //                         autoDelete: false,
        //    //                         arguments: null);

        //}

        public void Send(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            Channel.BasicPublish(exchange: ExchangeName,
                                 routingKey: RouteName,
                                 basicProperties: null,
                                 body: body);
        }
    }
}
