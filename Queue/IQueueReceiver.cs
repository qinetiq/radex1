﻿using System;

namespace Radex.Queue
{
    public interface IQueueReceiver
    {
        public event EventHandler<string> DataReceived;
    }
}
