﻿using System;

namespace Radex.Queue
{
    public class SignalDto
    {
        public int SignalStrength { get; set; }
        public string SourceId { get; set; }
        public DateTime TimeStamp { get; set; } = DateTime.Now;

    }
}
