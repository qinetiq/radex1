﻿using System;
using Newtonsoft.Json;

namespace Radex.Queue
{
    public class QProducer
    {
        public IQueue _queue { get; private set; }
        public QProducer(IQueue queue)
        {
            _queue = queue;
        }

        public void Send(string sourceId, int signal)
        {
            var dto = new SignalDto
            {
                SourceId = sourceId,
                SignalStrength = signal
            };

            var output = JsonConvert.SerializeObject(dto);
            _queue.Send(output);

            Console.WriteLine(" [x] Sent SourceId {0} Signal {1}", dto.SourceId, dto.SignalStrength);
        }

    }
}
