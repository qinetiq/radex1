﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Radex.Queue
{
    public class QueueConfig
    {
        public string Exchange { get; set; }
        public string Topic { get; set; }
    }
}
