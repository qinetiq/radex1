﻿

namespace Radex.Queue
{
    public interface IQueue
    {
        public void Send(string message);
    }
}
