﻿using Radex.Queue;
using Radex.Database;
using Radex.Model;
using System;

namespace Radex.DataConsumer
{
    internal class DataConsumer
    {
        public QConsumer Consumer { get; private set; }
        public RadexDatabase Database { get; private set; }

        public DataConsumer(DataConsumerConfig config, IQueueReceiver receiver)
        {
            Database = new RadexDatabase(config.Database);
            Consumer = new QConsumer(receiver);
            Consumer.DataReceived += Consumer_DataReceived;
        }

        private void Consumer_DataReceived(object sender, SignalDto e)
        {
            Console.WriteLine(e);
            Database.SaveData(new SignalData { Signal = e.SignalStrength, SourceId = e.SourceId});
        }
    }
}
