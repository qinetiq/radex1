﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Radex.DataConsumer;
using Radex.Queue;
using System;

namespace TestConsumer
{
    internal class ProgramConsumer
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Testing the Radex Consumer...");
            Console.WriteLine("Press any key to exit...");

            var builder = new ConfigurationBuilder()
                  .AddJsonFile($"appsettings.json", true, true);
            var config = builder.Build();
            var section = config.GetSection(nameof(DataConsumer));
            var consumerConfig = section.Get<DataConsumerConfig>();

            using IHost host = Host.CreateDefaultBuilder(args)
                .ConfigureServices((_, services) =>
                services.AddSingleton<IQueueReceiver, QueueReceiver>())
                .Build();

            _ = new DataConsumer(consumerConfig, new QueueReceiver(consumerConfig.Queue));
            Console.ReadLine();
        }
    }
}
