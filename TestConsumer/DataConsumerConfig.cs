﻿using Database;
using Radex.Queue;

namespace Radex.DataConsumer
{
    internal class DataConsumerConfig
    {
        public DatabaseConfig Database { get; set; }
        public QueueConfig Queue { get; set; }
    }
}
