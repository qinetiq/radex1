#pragma once

#ifdef RADEX_EXPORTS
#	define RADEX_API __declspec(dllexport) 
#else
#	define RADEX_API __declspec(dllimport) 
#endif

extern "C" RADEX_API int GetSignalStrength();

